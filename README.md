## Books

1. [Deep learning with Python by FRANÇOIS CHOLLET](https://github.com/gpk2000/deep_learning_resources/blob/master/Books/Deep%20Learning%20with%20Python.pdf)
    * This is the book from where i started learning about deep learning.
    * Covers useful concepts and example codes which makes it more intresting
    * I recommend this for whoever want to start with Deep Learning.
  
2. [Neural Networks and Deep Learning by Michael Nielsen](https://github.com/gpk2000/deep_learning_resources/blob/master/Books/neuralnetworksanddeeplearning.pdf)
    - This book is a bit more tough to understand than the above one (My opinion :slight_smile:)
    - This book has implementations from scratch
    - This is in my reading TODO list

3. [Neural Network Design](https://github.com/gpk2000/deep_learning_resources/blob/master/Books/NNDesign.pdf)
    - A bit more intense book.
    - More focus on mathematics side of Deep Learning
    - Rigorous theory and math
    - Not recommended for beginners
  
---

## Youtube Tutorials

1. [Intro to NNets by 3Blue1Brown](https://www.youtube.com/watch?v=aircAruvnKk&list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi)
    - The best introduction and motivation for learning Neural Networks
    
2. [MIT Introduction to Deep Learning | 6.S191](https://www.youtube.com/playlist?list=PLtBw6njQRU-rwp5__7C0oIVt26ZgjG9NI)
    - Interesting videos and lectures.
    - Their course website is [here](http://introtodeeplearning.com/)

---
